package dp.rundot.levelup.homework11;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Maksimovich Evgeniy on 31.10.2016.
 */
public class PhoneBook implements Serializable {

    private List<Record> records = new ArrayList<>();

    @Override
    public String toString() {
        return "PhoneBook{" +
                "records=" + records +
                '}';
    }

    public void addRecord(Record record) {
        record.setNumber((long)records.size() + 1);
        records.add(record);
    }

    public void removeRecord(Record record) {
        records.remove(record);
        if (record.getNumber() != records.size() + 2)
            records.stream()
                    .forEach(iterRecord -> iterRecord.setNumber((long)records.indexOf(iterRecord) + 1));
    }

    public boolean contains(Record record) {
        return records.contains(record);
    }

    public Record findByNumber(Long number) {
        return records.stream()
                .filter(record -> record.getNumber().equals(number))
                .findFirst()
                .get();
    }

    public List<Record> findByFullName(String name) {
        return records.stream()
                .filter(record -> record.getFirstName().contains(name))
                .collect(Collectors.toList());
    }

    public List<Record> findByPhoneNumber(String phoneNumber) {
        return records.stream()
                .filter(record -> record.getPhoneNumber().equals(phoneNumber))
                .collect(Collectors.toList());
    }

    public List<Record> findByAddress(String address) {
        return records.stream()
                .filter(record -> record.getAddress().contains(address))
                .collect(Collectors.toList());
    }

}
