package dp.rundot.levelup.homework11;

import java.io.*;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Maksimovich Evgeniy on 31.10.2016.
 */
public class Main {

    static PhoneBook phoneBook;
    private final static String DATAFILE = "phoneBook.txt";
    private final static String RESULTFILE = "searchResult.txt";

    public static void writeToFile() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(DATAFILE));
            out.writeObject(phoneBook);
        } catch (IOException e) {
            System.out.println("An I/O Exception");
            e.printStackTrace();
        }
    }

    public static PhoneBook readFromFile() {
        PhoneBook phoneBook = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(DATAFILE));
            phoneBook = (PhoneBook)in.readObject();
        } catch (FileNotFoundException e) {
            System.out.printf("File %s not found%n", DATAFILE);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("An I/O exception");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Can't deserialize object");
            e.printStackTrace();
        }
        return phoneBook;
    }

    public static void createRecord(BufferedReader reader) throws IOException {
        System.out.println("Creating new record:");
        Record record = new Record();
        System.out.print("Enter first name:\t");
        record.setFirstName(reader.readLine());
        System.out.print("Enter second name:\t");
        record.setSecondName(reader.readLine());
        System.out.print("Enter surname:\t");
        record.setSurname(reader.readLine());
        System.out.print("Enter phone number:\t");
        record.setPhoneNumber(reader.readLine());
        System.out.print("Enter address:\t");
        record.setAddress(reader.readLine());
        record.createFullName();
        if (phoneBook.contains(record))
            System.out.println("--- Such record already exists! ---");
        else
            phoneBook.addRecord(record);
    }

    public static void updateRecord(BufferedReader reader) throws IOException {
        System.out.print("Update record with number:\t");
        long num = 0;
        Record record = null;
        try {
            num = Long.parseLong(reader.readLine());
        } catch (NumberFormatException e) {
        }
        try {
            record = phoneBook.findByNumber(num);
        } catch (NoSuchElementException e) {
            System.out.println("--- No record with such number! ---");
            return;
        }
        String result;
        System.out.print("Enter new first name:\t");
        record.setFirstName((result = reader.readLine()).equals("")?record.getFirstName():result);
        System.out.print("Enter new second name:\t");
        record.setSecondName((result = reader.readLine()).equals("")?record.getSecondName():result);
        System.out.print("Enter new surname:\t");
        record.setSurname((result = reader.readLine()).equals("")?record.getSurname():result);
        System.out.print("Enter new phone number:\t");
        record.setPhoneNumber((result = reader.readLine()).equals("")?record.getPhoneNumber():result);
        System.out.print("Enter new address:\t");
        record.setAddress((result = reader.readLine()).equals("")?record.getAddress():result);
        record.createFullName();
    }

    public static void removeRecord(BufferedReader reader) throws IOException {
        System.out.print("Remove record with number:\t");
        long num = 0;
        Record record = null;
        try {
            num = Long.parseLong(reader.readLine());
        } catch (NumberFormatException e) {
        }
        try {
            record = phoneBook.findByNumber(num);
            phoneBook.removeRecord(record);
        } catch (NoSuchElementException e) {
            System.out.println("--- No record with such number! ---");
            return;
        }
    }

    public static void findRecord(BufferedReader reader) throws IOException {
        System.out.print("Find record by (1. Name, 2. PhoneNumber, 3. Address):\t");
        List<Record> result = null;
        String searchLine = "";
        String searchParam = "";
        switch (reader.readLine()) {
            case "1":
                System.out.print("Enter full name:\t");
                searchParam = "Searching by full name";
                result = phoneBook.findByFullName(searchLine = reader.readLine());
                break;
            case "2":
                System.out.print("Enter phone number:\t");
                searchParam = "Searching by phone number";
                result = phoneBook.findByPhoneNumber(searchLine = reader.readLine());
                break;
            case "3":
                System.out.print("Enter address:\t");
                searchParam = "Searching by address";
                result = phoneBook.findByAddress(searchLine = reader.readLine());
                break;
            default: return;
        }

        if (result.isEmpty()) {
            System.out.println("No records were found");
            return;
        }

        result.stream().forEach(System.out::println);

        System.out.print("Do you want to save search result (yes/no)?\t");
        if ("yes".equals(reader.readLine())) {
            BufferedWriter out = new BufferedWriter(new FileWriter(RESULTFILE));
            out.write(String.format("%s: %s", searchParam, searchLine));
            out.newLine();
            result.stream().forEach(record -> {
                try {
                    out.write(record.toString());
                    out.newLine();
                } catch (IOException e) {

                }
            });
            out.flush();
            out.close();
        }
    }

    public static void main(String[] args) throws IOException {

        phoneBook = readFromFile();
        System.out.println("--- Start of the program ---");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print("Enter the command: \t");
            switch (reader.readLine()) {
                case "exit":
                    writeToFile();
                    return;
                case "print":
                    System.out.println(phoneBook);
                    break;
                case "add":
                    createRecord(reader);
                    break;
                case "update":
                    updateRecord(reader);
                    break;
                case "remove":
                    removeRecord(reader);
                    break;
                case "find":
                    findRecord(reader);
                    break;
            }
        }

    }


}
