--- Первое задание
Создать базу данных с именем "bank" (в pgAdmin).
Создать в ней таблицы:

"customers" ( клиенты банка
	"id" - целое, не может быть пустым, является приватным ключом,
	"firstname" - текстовое длина 50, не может быть пустым,
	"lastname" - текстовое длина 50, не может быть пустым,
	"birthdate" - дата без времени, не может быть пустым,
	"address" - текстовое длина 45, не может быть пустым,
	"city" - текстовое длина 45, не может быть пустым,
	"passport" - текстовое длина 8, уникальное,
	"phone" - текстовое длина 14
);

CREATE TABLE customers (
    id SERIAL NOT NULL PRIMARY KEY,
    firstname VARCHAR(50) NOT NULL,
    lastname VARCHAR(50) NOT NULL,
    birthdate DATE NOT NULL,
    address VARCHAR(45) NOT NULL,
    city VARCHAR(45) NOT NULL,
    passport CHARACTER(8) UNIQUE,
    phone VARCHAR(14) );

CREATE TABLE "accounts" ( счета
	"accountnumber" - целое, не может быть пустым, является приватным ключом,
	"balance" - тип decimal, по умолчанию 0,
	"creationdate" дата со временем, не может быть пустым,
	"currency" - текстовое длина 3, по умолчанию 'UAN',
	"blocked" - логическое, по умолчанию FALSE,
	"customerid" внешний ключ к таблице "customers"
);

CREATE TABLE accounts (
    accountnumber SERIAL NOT NULL PRIMARY KEY,
    balance DECIMAL DEFAULT 0,
    creationdate TIMESTAMP NOT NULL,
    currency VARCHAR(3) DEFAULT 'UAN',
    blocked BOOLEAN DEFAULT FALSE,
    customerid INTEGER NOT NULL REFERENCES customers (id) );

CREATE TABLE "transactions" ( транзакции
	"id" - целое, не может быть пустым, является приватным ключом,
	"amount" - тип decimal, не может быть пустым
	"date" дата со временем, не может быть пустым,
	"operationtype" - текстовое длина 3 ('PUT', 'WSD')
	"accountnumber"  внешний ключ к таблице "accounts"
);

CREATE TABLE transactions (
    id SERIAL NOT NULL PRIMARY KEY,
    amount DECIMAL NOT NULL,
    date TIMESTAMP,
    operationtype VARCHAR(3),
    accountnumber INTEGER REFERENCES accounts (accountnumber) );