package dp.rundot.levelup.homework17.part2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 28.11.16.
 */
public class CustomersOperations {

    private static Connection connection = null;

    static {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", "levelup", "levelup");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    вспомогальный метод, выводит информацию о кастомере в консоль (в одной строке)
    private static void printCustomer(ResultSet result) {
        try {
            while (result.next()) {
                System.out.printf("Customer {" +
                        "id = %d; firstname = %s; lastname = %s; birthdate = %s; " +
                        "address = %s; city = %s; passport = %s; phone = %s;}%n",
                        result.getInt("id"),
                        result.getString("firstname"),
                        result.getString("lastname"),
                        result.getDate("birthdate"),
                        result.getString("address"),
                        result.getString("city"),
                        result.getString("passport"),
                        result.getString("phone"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//     выбирает все записи из таблицы "customers" и выводит их в консоль
    public static void selectAllCustomers() {
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM customers;");
            printCustomer(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    возвращает список айдишников кастомеров
    public static List<Long> selectCustomersId(Connection connection) {
        List<Long> customerIds = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT id FROM customers;");
            while (result.next()) {
                customerIds.add(result.getLong("id"));
            }
            statement.close();
            return customerIds;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

//    выводит в консоль кастомера, паспорт которого равен переданному (использовать prepareStatement)
    public static void selectCustomersByPassport(String passport) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM customers WHERE passport = ?;");
            statement.setString(1, passport);
            ResultSet result = statement.executeQuery();
            printCustomer(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    вставляет кастомера с одними и теми данными (константые данные, исп-ть createStatement)
    public static void constantInsert() {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(
                    "INSERT INTO customers (firstname, lastname, birthdate, address, city, passport, phone) " +
                            "VALUES ('John', 'Snow', '2001-01-01', 'Black Wall str., 1', 'Westeros', 'ABC12345', '+1234567890')" +
                            "ON CONFLICT DO NOTHING;");
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    вставляет кастомера с переданными данными
    public static void smartInsert(String firstName,String lastName,String birthdate,String address,String city,String passport,String phone) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO customers (firstname, lastname, birthdate, address, city, passport, phone)" +
                            "VALUES (?, ?, ?, ?, ?, ?, ?) " +
                            "ON CONFLICT DO NOTHING");
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setDate(3, Date.valueOf(birthdate));
            statement.setString(4, address);
            statement.setString(5, city);
            statement.setString(6, passport);
            statement.setString(7, phone);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

//    обновляет имя и фамилию кастомера с заданным id
    public static void updateCustomerFirstAndLastName(Long id, String firstName, String lastName) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE customers SET firstname = ?, lastname = ? WHERE id = ?;");
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setLong(3, id);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//     удаляет кастомера в заданным id
    public static void deleteCustomer(Long id) {
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM customers WHERE id = ?;");
            statement.setLong(1, id);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Method for testing purposes
    public static void main(String[] args) {
        //Adding constant record
        constantInsert();
        //Do nothing
        constantInsert();
        //Deleting record
        deleteCustomer(1l);
        //Adding constant record once more
        constantInsert();
        //Renaming existing customer
        updateCustomerFirstAndLastName(3l, "Max", "Payne");
        //Adding custom customer
        smartInsert("Luke", "Skywalker", "3001-01-01", "Tatooine, Sand desert str. 1", "Slave Market", "DEF12345", "+1234567890");
        //Do nothing. The same value of key field "passport"
        smartInsert("Luke", "Skywalker", "3001-01-01", "Tatooine, Sand desert str. 1", "Slave Market", "DEF12345", "+1234567890");
        //Print all records
        selectAllCustomers();
        //Print just Luke Skywalker
        selectCustomersByPassport("DEF12345");
        //Selecting ID's of all customers
        System.out.println(selectCustomersId(connection));
    }

}
