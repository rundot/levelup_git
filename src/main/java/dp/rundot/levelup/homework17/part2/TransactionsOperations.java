package dp.rundot.levelup.homework17.part2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 28.11.16.
 */
public class TransactionsOperations {

    private static Connection connection = null;
    static {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", "levelup", "levelup");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //  добавить для каждого счета запись о внесении ('PUT') 100 денежных единиц,
    //  время - текущее. Пользовать пакетную обработку запросов.
    public static void batchTransactionsInsert() {
        if (connection == null) return;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO transactions (amount, date, operationtype, accountnumber) " +
                            "VALUES (?, ?, ?, ?);");
            for (Long accountnumber : AccountsOperations.selectAccountsId(connection)) {
                preparedStatement.setLong(1, 100);
                preparedStatement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                preparedStatement.setString(3, "WSD");
                preparedStatement.setLong(4, accountnumber);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    //  для всех транзакций с типом операции 'PUT' установить поле amount = 150.
    //  Пользовать изменяющие методы ResultSet (update methods).
    public static void updateAllTransactions() {
        if (connection == null) return;
        try {
            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
            ResultSet resultSet = statement.executeQuery("SELECT * FROM transactions WHERE operationtype = 'PUT';");
            while (resultSet.next()) {
                resultSet.updateLong("amount", 150);
                resultSet.updateRow();
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //    удалить все транзакции с типом операции 'WSD'. Для всех удаленных транзакций, запомнить
    // номера счетов, для которых они были созданы. Для каждого такого номера счета создать и вставить
    // новую транзакцию с типом 'PUT', amount = 100, время текущее. Пользовать изменяющие
    // методы ResultSet (update methods).
    public static void deleteAllWithdraw() {
        if (connection == null) return;
        try {
            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
            ResultSet resultSet = statement.executeQuery("SELECT * FROM transactions WHERE operationtype = 'WSD';");
            List<Long> accountsId = new ArrayList<>();
            while (resultSet.next()) {
                accountsId.add(resultSet.getLong("accountnumber"));
                resultSet.deleteRow();
            }
            for (Long accountId : accountsId) {
                resultSet.moveToInsertRow();
                resultSet.updateLong("amount", 100);
                resultSet.updateTimestamp("date", new Timestamp(System.currentTimeMillis()));
                resultSet.updateString("operationtype", "PUT");
                resultSet.updateLong("accountnumber", accountId);
                resultSet.insertRow();
            }
            statement.executeBatch();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    //For testing purposes
    public static void main(String[] args) {
        //Adding new transactions;
        batchTransactionsInsert();
        //Updating amount for existing transactions;
        updateAllTransactions();
        //Removing WSD records and addping PUT records
        deleteAllWithdraw();
    }

}
