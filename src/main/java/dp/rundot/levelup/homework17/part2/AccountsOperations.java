package dp.rundot.levelup.homework17.part2;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 28.11.16.
 */
public class AccountsOperations {

    private static Connection connection = null;

    static {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", "levelup", "levelup");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //    вспомогальный метод, выводит информацию об аккаунте в консоль (в одной строке)
    private static void printAccount(ResultSet result) {
        if (connection == null) return;
        try {
            while (result.next()) {
                System.out.printf("Account {" +
                        "accountnumber = %d; balance = %s; creationdate = %s; currency = %s; blocked = %b; customerid = %d;}%n",
                        result.getLong("accountnumber"),
                        result.getBigDecimal("balance"),
                        result.getDate("creationdate"),
                        result.getString("currency"),
                        result.getBoolean("blocked"),
                        result.getLong("customerid"));            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //    выбирает все записи из таблицы "accounts" и выводит их в консоль
    public static void selectAccounts() {
        if (connection == null) return;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM accounts;");
            printAccount(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //    возвращает список айдишников аккаунтов
    public static List<Long> selectAccountsId(Connection connection) {
        if (connection == null) return null;
        try {
            List<Long> accountNumbers = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT accountnumber FROM accounts");
            while (result.next()) {
                    accountNumbers.add(result.getLong("accountnumber"));}
            statement.close();
            return accountNumbers;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    //    вставляет аккаунт с переданными данными
    public static void smartInsert(BigDecimal balance, String creationDate, String currency, boolean blocked, Long userId) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO accounts (balance, creationDate, currency, blocked, customerid) " +
                    "VALUES (?, ?, ?, ?, ?);");
            statement.setBigDecimal(1, balance);
            statement.setTimestamp(2, new Timestamp(Date.valueOf(creationDate).getTime()));
            statement.setString(3, currency);
            statement.setBoolean(4, blocked);
            statement.setLong(5, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    //    создает счет для каждого существующего пользователя (CustomersOperations.selectCustomersId - поможет
    // получить список айдишников). Счет гривневый, нулевой, не заблокированный, дата создания - текущая на момент
    // вставки (java.time.LocalDateTime.now()).
    // Пользовать пакетную обработку запросов (сначала созаем все запросы
    // на вставку, потом выполняем пачкой).
    public static void batchAccountsInsert() {
        if (connection == null) return;
        List<Long> customerIds = CustomersOperations.selectCustomersId(connection);
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO accounts (balance, creationDate, currency, blocked, customerid) " +
                    "VALUES (?, ?, ?, ?, ?);");
            for (Long customerId : customerIds) {
                statement.setBigDecimal(1, BigDecimal.ZERO);
                statement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                statement.setString(3, "UAH");
                statement.setBoolean(4, false);
                statement.setLong(5, customerId);
                statement.addBatch();
            }
            statement.executeBatch();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }



    public static void main(String[] args) {
        //Creates an account for each customer
        batchAccountsInsert();
        //Prints all accounts
        selectAccounts();
        //Adding custom account
        smartInsert(BigDecimal.valueOf(3000l), "2016-01-01", "USD", false, 13l);
        //Print all accounts
        selectAccounts();
        //Print accounts ID'
        System.out.println(selectAccountsId(connection));
    }

}
