package dp.rundot.levelup.homework18;

import dp.rundot.levelup.homework18.service.AccountService;
import dp.rundot.levelup.homework18.service.CustomerService;
import dp.rundot.levelup.homework18.service.TransactionService;

/**
 * Created by rundot on 09.12.2016.
 */
public class Main {
    public static void main(String[] args) {
        AccountService accountService = new AccountService();
        CustomerService customerService = new CustomerService();
        TransactionService transactionService = new TransactionService();
        System.out.println(accountService.list());
        System.out.println(customerService.list());
        System.out.println(transactionService.list());
    }
}
