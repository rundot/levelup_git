package dp.rundot.levelup.homework18.dto;

import dp.rundot.levelup.homework18.domain.Account;
import dp.rundot.levelup.homework18.domain.Transaction;

import java.util.List;

/**
 * Created by emaksimovich on 02.12.16.
 */
public class AccountDto extends Account {

    private List<Transaction> transactions;

    public AccountDto(Account account, List<Transaction> transactions) {
        super(account.getAccountNumber(),
                account.getBalance(),
                account.getCreationDate(),
                account.getCurrency(),
                account.isBlocked(),
                account.getCustomerId());
        this.transactions = transactions;
    }

}
