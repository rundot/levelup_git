package dp.rundot.levelup.homework18.dao;

import dp.rundot.levelup.homework18.domain.Account;

import java.util.List;

/**
 * Created by emaksimovich on 30.11.16.
 */
public interface AccountDao {

    Account get(Long accountNumber);

    Long save(Account account);

    void update(Account account);

    void delete(Account account);

    List<Account> list();

    void deleteByCustomer(Long customerId);
}
