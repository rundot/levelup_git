package dp.rundot.levelup.homework18.dao;

import dp.rundot.levelup.homework18.domain.Transaction;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class TransactionDaoImpl implements TransactionDao {

    private static Connection connection = null;

    static {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", "levelup", "levelup");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Transaction get(Long id) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM transactions WHERE id = ?;");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Transaction transaction = new Transaction(
                    resultSet.getLong("id"),
                    resultSet.getBigDecimal("amount"),
                    resultSet.getTimestamp("date"),
                    resultSet.getString("operationtype"),
                    resultSet.getLong("accountnumber"));
            resultSet.close();
            statement.close();
            return transaction;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long save(Transaction transaction) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO transactions (amount, date, operationtype, accountnumber) " +
                            "VALUES (?, ?, ?, ?) " +
                            "RETURNING id;"
            );
            statement.setBigDecimal(1, transaction.getAmount());
            statement.setTimestamp(2, transaction.getDate());
            statement.setString(3, transaction.getOperationType());
            statement.setLong(4, transaction.getAccountNumber());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Long result = resultSet.getLong("id");
            resultSet.close();
            statement.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Transaction transaction) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE transactions SET " +
                            "amount = ?, " +
                            "date = ?, " +
                            "operationtype = ?, " +
                            "accountnumber = ? " +
                            "WHERE id = ?;");
            statement.setBigDecimal(1, transaction.getAmount());
            statement.setTimestamp(2, transaction.getDate());
            statement.setString(3, transaction.getOperationType());
            statement.setLong(4, transaction.getAccountNumber());
            statement.setLong(5, transaction.getId());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Transaction transaction) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM transactions WHERE  id = ?;");
            statement.setLong(1, transaction.getId());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Transaction> list() {
        if (connection == null) return null;
        try {
            Statement statement = connection.createStatement();
            List<Transaction> transactionList = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM transactions;");
            while (resultSet.next()) {
                Transaction transaction = new Transaction(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("amount"),
                        resultSet.getTimestamp("date"),
                        resultSet.getString("operationtype"),
                        resultSet.getLong("accountnumber"));
                transactionList.add(transaction);
            }
            resultSet.close();
            statement.close();
            return transactionList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteByAccount(Long accountId) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM transactions WHERE accountnumber = ?;");
            statement.setLong(1, accountId);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
