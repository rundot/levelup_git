package dp.rundot.levelup.homework18.dao;

import dp.rundot.levelup.homework18.domain.Transaction;

import java.util.List;

/**
 * Created by emaksimovich on 30.11.16.
 */
public interface TransactionDao {

    Transaction get(Long Id);

    Long save(Transaction Transaction);

    void update(Transaction Transaction);

    void delete(Transaction Transaction);

    List<Transaction> list();

    void deleteByAccount(Long accountId);

}
