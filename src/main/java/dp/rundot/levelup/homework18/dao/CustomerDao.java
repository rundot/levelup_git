package dp.rundot.levelup.homework18.dao;

import dp.rundot.levelup.homework18.domain.Customer;
import java.util.List;

/**
 * Created by emaksimovich on 30.11.16.
 */
public interface CustomerDao {

    Customer get(Long Id);

    Long save(Customer customer);

    void update(Customer customer);

    void delete(Customer customer);

    List<Customer> list();

}
