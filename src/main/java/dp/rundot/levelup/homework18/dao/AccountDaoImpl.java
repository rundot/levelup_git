package dp.rundot.levelup.homework18.dao;

import dp.rundot.levelup.homework18.domain.Account;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class AccountDaoImpl implements AccountDao {

    private static Connection connection = null;

    static {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", "levelup", "levelup");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Account get(Long accountNumber) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM accounts WHERE accountnumber = ?;");
            statement.setLong(1, accountNumber);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Account account = new Account(
                    resultSet.getLong("accountnumber"),
                    resultSet.getBigDecimal("balance"),
                    resultSet.getTimestamp("creationdate"),
                    resultSet.getString("currency"),
                    resultSet.getBoolean("blocked"),
                    resultSet.getLong("customerid")
            );
            resultSet.close();
            statement.close();
            return account;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long save(Account account) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO accounts (balance, creationdate, currency, blocked, customerid) " +
                            "VALUES (?, ?, ?, ?, ?) " +
                            "RETURNING id;");
            statement.setBigDecimal(1, account.getBalance());
            statement.setTimestamp(2, account.getCreationDate());
            statement.setString(3, account.getCurrency());
            statement.setBoolean(4, account.isBlocked());
            statement.setLong(5, account.getCustomerId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Long result = resultSet.getLong("accountnumber");
            resultSet.close();
            statement.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Account account) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE accounts SET " +
                            "balance = ?, " +
                            "creationdate = ?, " +
                            "currency = ?, " +
                            "blocked = ?, " +
                            "customerid = ? " +
                            "WHERE accountnumber = ?;");
            statement.setBigDecimal(1, account.getBalance());
            statement.setTimestamp(2, account.getCreationDate());
            statement.setString(3, account.getCurrency());
            statement.setBoolean(4, account.isBlocked());
            statement.setLong(5, account.getCustomerId());
            statement.setLong(6, account.getAccountNumber());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Account account) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM accounts WHERE accountnumber = ?;");
            statement.setLong(1, account.getAccountNumber());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Account> list() {
        if (connection == null) return null;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM accounts;");
            List<Account> accounts = new ArrayList<>();
            while (resultSet.next()) {
                Account account = new Account(
                        resultSet.getLong("accountnumber"),
                        resultSet.getBigDecimal("balance"),
                        resultSet.getTimestamp("creationdate"),
                        resultSet.getString("currency"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getLong("customerid")
                );
                accounts.add(account);
            }
            resultSet.close();
            statement.close();
            return accounts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteByCustomer(Long customerId) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM accounts WHERE customerid = ?;");
            statement.setLong(1, customerId);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
