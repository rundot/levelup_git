package dp.rundot.levelup.homework18.domain;

import java.sql.Date;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class Customer {

    private long id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String address;
    private String city;
    private String passport;
    private String phone;

    public Customer(long id, String firstName, String lastName, Date birthDate, String address, String city, String passport, String phone) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.address = address;
        this.city = city;
        this.passport = passport;
        this.phone = phone;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getPassport() {
        return passport;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", passport='" + passport + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
