package dp.rundot.levelup.homework18.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class Transaction {

    private long id;
    private BigDecimal amount;
    private Timestamp date;
    private String operationType;
    private long accountNumber;

    public Transaction(long id, BigDecimal amount, Timestamp date, String operationType, long accountNumber) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.operationType = operationType;
        this.accountNumber = accountNumber;
    }

    public long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Timestamp getDate() {
        return date;
    }

    public String getOperationType() {
        return operationType;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    @Override
    public String toString() {

        return "Transaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", date=" + date +
                ", operationType='" + operationType + '\'' +
                ", accountNumber=" + accountNumber +
                '}';
    }
}
