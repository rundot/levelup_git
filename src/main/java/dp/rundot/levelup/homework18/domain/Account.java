package dp.rundot.levelup.homework18.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class Account {

    private long accountNumber;
    private BigDecimal balance;
    private Timestamp creationDate;
    private String currency;
    private boolean blocked;
    private long customerId;

    public Account(long accountNumber, BigDecimal balance, Timestamp creationDate, String currency, boolean blocked, long customerId) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.creationDate = creationDate;
        this.currency = currency;
        this.blocked = blocked;
        this.customerId = customerId;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public String getCurrency() {
        return currency;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public long getCustomerId() {
        return customerId;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", balance=" + balance +
                ", creationDate=" + creationDate +
                ", currency='" + currency + '\'' +
                ", blocked=" + blocked +
                ", customerId=" + customerId +
                '}';
    }
}
