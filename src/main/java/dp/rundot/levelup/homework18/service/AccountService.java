package dp.rundot.levelup.homework18.service;

import dp.rundot.levelup.homework18.dao.AccountDao;
import dp.rundot.levelup.homework18.dao.AccountDaoImpl;
import dp.rundot.levelup.homework18.dao.TransactionDaoImpl;
import dp.rundot.levelup.homework18.dao.TransactionDao;
import dp.rundot.levelup.homework18.domain.Account;
import dp.rundot.levelup.homework18.domain.Transaction;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class AccountService {

    private static AccountDao accountDao = new AccountDaoImpl();

    public void save(Account account) {
        accountDao.save(account);
        if (account.getBalance() != BigDecimal.ZERO) {
            Transaction transaction = new Transaction(
                    0l,
                    account.getBalance(),
                    new Timestamp(System.currentTimeMillis()),
                    "PUT",
                    account.getAccountNumber());
        }
    }

    public void delete(Account account) {
        accountDao.delete(account);
        TransactionDao transactionDao = new TransactionDaoImpl();
        transactionDao.deleteByAccount(account.getAccountNumber());
    }

    public List<Account> list() {
        return accountDao.list();
    }
}
