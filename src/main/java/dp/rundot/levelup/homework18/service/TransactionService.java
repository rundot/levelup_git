package dp.rundot.levelup.homework18.service;

import dp.rundot.levelup.homework18.dao.TransactionDaoImpl;

import java.util.List;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class TransactionService {

    private static TransactionDaoImpl transactionDao = new TransactionDaoImpl();

    public List<dp.rundot.levelup.homework18.domain.Transaction> list() {
        return transactionDao.list();
    }

}
