package dp.rundot.levelup.homework18.service;

import dp.rundot.levelup.homework18.dao.*;
import dp.rundot.levelup.homework18.domain.Account;
import dp.rundot.levelup.homework18.domain.Customer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class CustomerService {

    private static CustomerDao customerDao = new CustomerDaoImpl();

    public void save(Customer customer) {
        customerDao.save(customer);
        AccountDao accountDao = new AccountDaoImpl();
        accountDao.save(new Account(0l,
                BigDecimal.ZERO,
                new Timestamp(System.currentTimeMillis()),
                "UAH",
                false,
                customer.getId()));
    }

    public void delete(Customer customer) {
        customerDao.delete(customer);
        AccountDao accountDao = new AccountDaoImpl();
        accountDao.deleteByCustomer(customer.getId());
        accountDao.list().stream()
                .filter(account -> (account.getCustomerId() == customer.getId()))
                .collect(Collectors.toList())
                .stream()
                .forEach(account -> new AccountService().delete(account));
    }

    public List<Customer> list() {
        return customerDao.list();
    }

}
