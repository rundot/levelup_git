package dp.rundot.levelup.homework5.part2;

/**
 * Created by rundot on 05.10.2016.
 */
public class Test<T> {

    public static void main(String[] args) {
        int a = 0b110010101;
//        System.out.printf("Original number: %s%n", Integer.toBinaryString(a));
//        System.out.printf("bit %d inverted: %s%n", 4, Integer.toBinaryString(invertNthBit(a, 4)));
//        System.out.printf("bit %d inverted: %s%n", 3, Integer.toBinaryString(invertNthBit(a, 3)));
        System.out.println(1<<1);
    }

    public static int invertNthBit(int a, int n) {
        return  a ^ (1 << n - 1);
    }

}
