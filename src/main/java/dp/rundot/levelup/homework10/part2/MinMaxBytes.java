package dp.rundot.levelup.homework10.part2;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Maksimovich Evgeniy on 24.10.2016.
 */
public class MinMaxBytes {


    //Read filename from console. Find bytes with minimal and maximal entries

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        Map<Integer, Integer> fileBytes = new HashMap<>();
        FileInputStream in = new FileInputStream(filename);
        while (in.available() > 0) {
            int b = in.read();
            fileBytes.put(b, fileBytes.get(b) == null? 1 : fileBytes.get(b) + 1);
        }
        int minByte = 0;
        int maxByte = 0;
        int min = Integer.MAX_VALUE;
        int max = 0;

        for (Map.Entry<Integer, Integer> entry : fileBytes.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                maxByte = entry.getKey();
            }
            if (entry.getValue() < min) {
                min = entry.getValue();
                minByte = entry.getKey();
            }
        }
        reader.close();
        in.close();
        System.out.printf("Minimum %d, Maximum %d%n", minByte, maxByte);


    }

}
