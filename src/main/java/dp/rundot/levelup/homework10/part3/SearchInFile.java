package dp.rundot.levelup.homework10.part3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Maksimovich Evgeniy on 24.10.2016.
 */
public class SearchInFile {

    //Read filename and word from console. Find all the lines from file containing given word and add them to a List. Use Scanner.

    public static void main(String[] args) {
        Scanner consoleScanner = new Scanner(System.in);
        System.out.print("Enter filename:\t");
        String filename = consoleScanner.nextLine();
        System.out.print("Enter sequence to search for:\t");
        String searchedLine = consoleScanner.nextLine();
        consoleScanner.close();

        List<String> searchedLines = new ArrayList<>();
        try (Scanner fileScanner = new Scanner(new FileReader(filename))) {
            while (fileScanner.hasNext()) {
                String line = fileScanner.nextLine();
                if (line.contains(searchedLine))
                    searchedLines.add(line);
            }
            System.out.println("---searching result---");
            searchedLines.forEach(System.out::println);
        } catch (FileNotFoundException e) {
            System.out.printf("File %s is not found%n", filename);
            e.printStackTrace();
        }
    }


}
