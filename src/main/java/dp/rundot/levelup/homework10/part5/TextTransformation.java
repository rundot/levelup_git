package dp.rundot.levelup.homework10.part5;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Maksimovich Evgeniy on 24.10.2016.
 */
public class TextTransformation {

    //Read from console two filenames. First file contains set of words separated by space.
    // Write to second file all the words with odd letters number

    public static void main(String[] args){

        try {
            Scanner consoleScanner = new Scanner(System.in);
            System.out.print("Enter source filename: \t");
            String source = consoleScanner.nextLine();
            System.out.print("Enter destination filename: \t");
            String destination = consoleScanner.nextLine();
            Scanner in = new Scanner(new FileReader(source));
            FileWriter out = new FileWriter(destination);

            while (in.hasNext()) {
                String[] words = in.nextLine().split("\\s+");
                for (String word : words) {
                    if (word.length() % 2 == 0) {
                        String outWord = Character.toUpperCase(word.charAt(0)) + word.substring(1) + ", ";
                        out.write(outWord.toCharArray());
                    }
                }
            }
            in.close();
            out.close();
            consoleScanner.close();
        } catch (IOException e) {
            System.out.println("An I/O Exception");
            e.printStackTrace();
        }
    }

}
