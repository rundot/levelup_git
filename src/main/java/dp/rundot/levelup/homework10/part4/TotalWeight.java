package dp.rundot.levelup.homework10.part4;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Maksimovich Evgeniy on 24.10.2016.
 */
public class TotalWeight {


    //Read filename from console. All the lines in given file contain animal and its weight separated by space.
    //Print to console animal with the biggest weight

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter filename:\t");
        try {
            reader.close();
            String filename = reader.readLine();
            BufferedReader in = new BufferedReader(new FileReader(filename));
            Map<String, Integer> animals = new TreeMap<>();
            while (in.ready()) {
                String[] line = in.readLine().split("\\s+");
                String animal = line[0];
                Integer weight = Integer.parseInt(line[1]);
                animals.put(animal, animals.get(animal) == null ? weight : animals.get(animal) + weight);
            }
            in.close();
            int maxWeight = 0;
            String theMostHeavyAnimal = "";
            for (Map.Entry<String, Integer> entry : animals.entrySet()) {
                if (entry.getValue() > maxWeight) {
                    maxWeight = entry.getValue();
                    theMostHeavyAnimal = entry.getKey();
                }
            }
            System.out.printf("%s %d kg", theMostHeavyAnimal, maxWeight);
        } catch (FileNotFoundException e) {
            System.out.println("File is not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("An I/O exception");
            e.printStackTrace();
        }

    }

}
