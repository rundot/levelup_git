package dp.rundot.levelup.homework10.part1;

import java.io.*;

/**
 * Created by Maksimovich Evgeniy on 24.10.2016.
 */
public class CopyFiles {

    //Read two filenames from console, Copy second file to first using FileOutputStream, FileInputStream

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("Enter destination filename:\t");
            String destination = reader.readLine();
            System.out.print("Enter source filename:\t");
            String source = reader.readLine();
            FileInputStream in = new FileInputStream(source);
            FileOutputStream out = new FileOutputStream(destination);
            System.out.printf("Copying %s to %s...", source, destination);
            byte[] sourceContent = new byte[in.available()];
            in.read(sourceContent);
            out.write(sourceContent);
            in.close();
            out.close();
            reader.close();
            System.out.println("Complete");
        } catch (FileNotFoundException e) {
            System.out.println("Can't find file");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("An I/O exception");
            e.printStackTrace();
        }
    }

}
