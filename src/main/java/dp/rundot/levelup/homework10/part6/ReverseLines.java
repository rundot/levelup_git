package dp.rundot.levelup.homework10.part6;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Maksimovich Evgeniy on 24.10.2016.
 */
public class ReverseLines {

    //Read two filenames from console. Read lines from source file, reverse and write them to destination file;

    public static void main(String[] args) {

        Scanner consoleScanner = new Scanner(System.in);
        System.out.print("Enter source filename:\t");
        String source = consoleScanner.nextLine();
        System.out.print("Enter destination filename:\t");
        String destination = consoleScanner.nextLine();
        consoleScanner.close();

        try (Scanner in = new Scanner(new FileReader(source)); BufferedWriter out = new BufferedWriter(new FileWriter(destination))) {
            while (in.hasNext()) {
                String line = in.nextLine();
                out.write((new StringBuffer(line)).reverse().toString());
                out.newLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("An I/O exception");
            e.printStackTrace();
        }
    }

}
