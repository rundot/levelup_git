package dp.rundot.levelup.homework14;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;

/*
* Размытие изображения
* */
public class ImageBlurWithExecutorService {
    private static int imgSize;
    //Число пикселей в маске (13 - значит справа и слева по 6)
    private static int mBlurWidth = 13;

    public static void main(String[] args) {
        try {
            System.out.println("Loading image...");

            BufferedImage lImg = ImageIO.read(new File("C:/Temp/cat.jpg"));
            System.out.println("Image loaded.");

            imgSize = lImg.getWidth();

            System.out.println("Processing image...");
            BufferedImage rImg = process(lImg);
            System.out.println("Image processed");

            System.out.println("Saving image...");
            ImageIO.write(rImg, "jpg", new File("C:/Temp/out.jpg"));
            System.out.println("Image saved");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static BufferedImage process(BufferedImage lImg) throws ExecutionException, InterruptedException {
        //представляем изображение, как массив пикселей ()
        int[] rgb = imgToRgb(lImg);

        long start = System.currentTimeMillis();
        //задаем число потоков и задач
        int[] transformed = blurParallel(rgb, 4, 4);
        long end = System.currentTimeMillis();

        System.out.println((end - start) + "ms");

        //переводим массив пикселей в изображение
        return rgbToImg(transformed);
    }

    //Consumer just for getting Future value avoiding exceptions
    private static Consumer futureConsume  = future -> {
        try {
            ((Future)future).get();
        } catch (Exception e) {
        }};

    private static int[] blurParallel(int[] rgb, int threadsCount, int tasksCount) throws ExecutionException, InterruptedException {

        //результирующий массив
        final int[] res = new int[rgb.length];
        final int partSize = rgb.length / tasksCount;

        ExecutorService service = Executors.newFixedThreadPool(4);

        Collection<Future> resultList = new ArrayList<>();

        for (int i = 0; i < tasksCount; i++) {
            final int j = i;
            resultList.add(service.submit(() -> {computeDirectly(rgb, j * partSize, (j + 1) * partSize, res);}));
        }

        resultList.stream().forEach(futureConsume);

        service.shutdown();

        return res;
    }

    static protected void computeDirectly(int[] source, int start, int length, int[] destination) {

        for (int i = start; i < start + length; i++) {
            List<Integer> neighboringPixels = new ArrayList<>();
            int row = i / imgSize;
            int col = i % imgSize;
            int delta = mBlurWidth / 2;

            for (int numRow = row - delta > 0 ? row - delta : 0; numRow < (row + delta > imgSize ? imgSize : row + delta); numRow++) {
                int[] subArray = Arrays.copyOfRange(source,
                        numRow * imgSize + (col - delta > 0 ? col - delta : 0),
                        numRow * imgSize + (col + delta > imgSize - 1 ? imgSize - 1: col + delta));
                Arrays.stream(subArray).forEach(num -> neighboringPixels.add(num));
            }
            destination[i] = processPixel(neighboringPixels);
        }
    }

    private static int processPixel(List<Integer> neighboringPixels) {
        int blueComponent = 0;
        int greenComponent = 0;
        int redComponent = 0;
        for (Integer pixel : neighboringPixels) {
            blueComponent += pixel & 0x00_00_00_ff;
            greenComponent += (pixel & 0x00_00_ff_00) >> 8;
            redComponent += (pixel & 0x00_ff_00_00) >> 16;
        }
        int resultBlue = blueComponent / neighboringPixels.size();
        int resultGreen = greenComponent / neighboringPixels.size();
        int resultRed = redComponent / neighboringPixels.size();

        return (resultRed << 16) + (resultGreen << 8) + resultBlue;
    }

    private static int[] imgToRgb(BufferedImage img) {
        int[] res = new int[imgSize * imgSize];
        img.getRGB(0, 0, imgSize, imgSize, res, 0, imgSize);
        return res;
    }

    private static BufferedImage rgbToImg(int[] rgb) {
        BufferedImage res = new BufferedImage(imgSize, imgSize, BufferedImage.TYPE_INT_RGB);
        res.setRGB(0, 0, imgSize, imgSize, rgb, 0, imgSize);
        return res;
    }
}
