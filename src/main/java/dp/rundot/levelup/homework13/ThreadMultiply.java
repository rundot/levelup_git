package dp.rundot.levelup.homework13;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Created by rundot on 01.11.2016.
 */
public class ThreadMultiply implements Callable {

    private int from;
    private int to;
    private static final int MATRIX_SIZE = 200;
    private static long[][] a;
    private static long[][] b;
    private static long executionTime;

    public ThreadMultiply(int from, int to) {
        this.from = from;
        this.to = to;
    }

    private long[][] multiply(int from, int to) {
        long[][] result = new long[to - from + 1][b[0].length];
        for (int i = from; i <= to; i++) {
            for (int j = 0; j < a[0].length; j++) {
                int n = 0;
                for (int k = 0; k < b.length; k++) {
                    n += a[i][k] * b[k][j];
                }
                result[i - from][j] = n;
            }
        }
        return result;
    }


    //Multiply given matrix in several parallel threads

    public static long[][] multiplyParallel(long[][] a, long[][] b, int threadsQty, ExecutorService service) {
        long startTime = System.currentTimeMillis();
        ThreadMultiply.a = a;
        ThreadMultiply.b = b;

        //Each task calculates some lines of final matrix. We need tasks no more than matrix length
        if (threadsQty > a.length) threadsQty = a.length;

        //Number of lines each task calculates
        int delta = a.length / threadsQty;

        //Creating task list
        List<Callable<long[][]>> taskList = new ArrayList<>();

        //Fill it with tasks
        for (int j = 0; j < a.length; j += delta) {
            int toIndex = (j + delta) > a.length ? a.length - 1 : j + delta - 1;
            Callable task = new ThreadMultiply(j, toIndex);
            taskList.add(task);
        }

        long[][] result = null;

        //Starting tasks and collecting result from them
        try {
            result = service
                    .invokeAll(taskList)
                    .stream()
                    .flatMap(future -> {
                        try {
                            return Arrays.asList(future.get()).stream();
                        } catch (Exception e) {
                            return null;
                        }
                    })
                    .collect(Collectors.toList())
                    .toArray(new long[MATRIX_SIZE][MATRIX_SIZE]);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        service.shutdown();

        executionTime = System.currentTimeMillis() - startTime;

        return result;
    }

    @Override
    public long[][] call() throws Exception {
        return multiply(from, to);
    }

    public static void main(String[] args) throws InterruptedException{

        int threadsQty = 16;

        List<ExecutorService> services = Arrays.asList(Executors.newFixedThreadPool(8),
                Executors.newCachedThreadPool(),
                Executors.newSingleThreadExecutor(),
                Executors.newWorkStealingPool(8));

        Map<String, Long> result = new TreeMap<>();

        services.stream().forEach(service -> {
            long[][] a = MatrixHelper.fillMatrix(new long[MATRIX_SIZE][MATRIX_SIZE]);
            long[][] b = MatrixHelper.fillMatrix(new long[MATRIX_SIZE][MATRIX_SIZE]);
            multiplyParallel(a, b, threadsQty, service);
            result.put(service.getClass().getName(), executionTime);
        });

        result.forEach((key, value) -> {
            System.out.printf("Execution service: %s - %dms%n", key, value);
        });

    }

}
