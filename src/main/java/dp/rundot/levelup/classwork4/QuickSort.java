package dp.rundot.levelup.classwork4;

/**
 * Created by rundot on 08.11.2016.
 */
public class QuickSort {


    private static int[] array;

    public static void sort(int[] array) {
        sort(array, 0, array.length - 1);
    };

    public static void sort(int[] array, int from , int to) {
        if (from >= to) return;
        int n = partition(array, from, to);
        sort(array, from, n - 1);
        sort(array, n + 1, to);
    }

    public static int partition(int[] array, int from, int to) {
        int result = array[from];
        int i = from;
        int j = to + 1;
        while (true){
            while (array[++i] < result) {
                if (i == to) break;
            }
            while (array[--j] > result) {
                if (i == from) break;
            }
            if (i >= j) break;
            int t = array[i];
            array[i] = array[j];
            array[j] = t;
        }
        int t = array[j];
        array[j] = result;
        array[from] = t;
        return j;
    }

    public static void main(String[] args) {

    }


}
