package dp.rundot.levelup.homework7;

/**
 * Created by Maksimovich Evgeniy on 10.10.2016.
 */
public enum Operation {

    PUT,
    WITHDRAW;

}
