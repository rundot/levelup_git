package dp.rundot.levelup.homework7;

/**
 * Created by Maksimovich Evgeniy on 10.10.2016.
 */
public class Transaction {

    private Account source;
    private Operation operation;
    private double amount;

    public Transaction(Account source, Operation operation, double amount) {
        this.source = source;
        this.operation = operation;
        this.amount = amount;
    }

    public Account getSource() {
        return source;
    }

    @Override
    public String toString() {
        return String.format("Transaction: account number %d, operation = %s, amount = %.2f;", source.getAccountNumber(), operation, amount);
    }
}
