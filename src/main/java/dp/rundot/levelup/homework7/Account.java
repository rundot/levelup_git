package dp.rundot.levelup.homework7;

/**
 * Created by Maksimovich Evgeniy on 10.10.2016.
 */
public class Account implements Comparable{

    private long accountNumber;
    private double balance;
    private static long GLOBAL_ACCOUNT_NUMBER = 1_000_000L;

    public Account(double balance) {
        this.balance = balance;
        this.accountNumber = GLOBAL_ACCOUNT_NUMBER++;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (accountNumber != account.accountNumber) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return  (int) (accountNumber ^ (accountNumber >>> 32));
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return String.format("Account %d, balance = $%.2f;", accountNumber, balance);
    }

    public void increaseBalance(Double sum) {
        balance += sum;
    }

    public void decreaseBalance(Double sum) {
        balance -= sum;
    }

    @Override
    public int compareTo(Object o) {
        return Long.compare(this.accountNumber, ((Account)o).getAccountNumber());
    }
}
