 function getGreeting(date) {
    var h = date.getHours();
    if (h > 5 && h < 12) {
        return "Good Morning!";
    }
    if (h >= 12 && h < 18) {
        return "Good Afternoon!";
    }
    return "Good Night!";
}

window.onload = function() {
    var date = new Date();
    document.getElementById("greeting").innerHTML = getGreeting(date) + " May the Force be with You!";
}