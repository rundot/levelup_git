package dp.rundot.levelup.classwork10;

import javax.swing.*;
import java.io.*;

/**
 * Created by emaksimovich on 24.10.16.
 */
public class FileParser {

    public static File getFile() {
        JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
        int result = chooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION)
            return chooser.getSelectedFile();
        return null;
    }

    public static void main(String[] args) throws IOException {
        File file = getFile();

        FileReader reader = new FileReader(file);
        int charECnt = 0;

        while (reader.ready()) {
            if ((char)reader.read() == 'e') charECnt++;
        }
        JOptionPane.showInputDialog("Enter character to look for");
        JOptionPane.showMessageDialog(null, String.format("Found %d entries of char 'e' in file %s", charECnt, file.getName() ));

    }

}
