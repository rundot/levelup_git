package dp.rundot.levelup.homework1;

/**
 * Created by rundot on 08.11.2016.
 */
public class HelloWorld {

    public static void main(String[] args) {
        String firstName = "Evgeniy";
        String lastName = "Maksimovich";
        System.out.printf("My name is %s %s. It's very boring homework!", lastName, firstName);
    }

}
