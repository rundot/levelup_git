package dp.rundot.levelup.homework2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by rundot on 08.11.2016.
 */
public class HomeWork {


    // Найти минимальное из трех чисел (значения чисел любые)
    public static int minOfThree(int a, int b, int c) {
        return a < b ? a < c ? a : c : b < c ? b : c;
    }

    //  Написать расчет факториала в 3х вариантах: с использованием while
    public static long factorialMethod1 (int a) {
        long result = 1;
        int num = 0;
        while (num < a) {
            result *= ++num;
        }
        return result;
    }

    //  Написать расчет факториала в 3х вариантах: с использованием do-while
    public static long factorialMethod2 (int a) {
        long result = 1;
        int num = 0;
        do {
            result *= ++num;
        } while (num < a);
        return result;
    }

    // Написать расчет факториала в 3х вариантах: с использованием for
    public static long factorialMethod3 (int a) {
        long result = 1;
        for (int i = 1; i <= a; i++) {
            result *= i;
        }
        return result;
    }

    // Создать одномерный массив (который кроме прочих чисел содержит как минимум три числа 5). Вывести в консоль все элементы, не равные 5
    public static String getArrayElementsExceptFive() {
        String result = "";
        StringBuilder sb = new StringBuilder();
        int[] array = {1, 5, 2, 5, 3, 5, 4, 5, 5, 5, 6, 5};
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 5) {
                sb.append(array[i]).append(" ");
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    // Написать "калькулятор". Заданы 2 числа ввести с клавиатуры один из символов: "+", "-", "*", "/".
    // Применить введенную операцию к числам, вывести результат в консоль.
    // Например: задано числа a = 15. b = 3.
    // С клавиатуры вводим символ "+", в консоль выводится "Result of plus operation is 18".
    public static void dummyCalculator () throws IOException {
        int a = 15;
        int b = 3;
        String result = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String action = reader.readLine();
        switch (action) {
            case "+": result = String.format("Result of addition is %d", a + b); break;
            case "-": result = String.format("Result of subtraction is %d", a - b); break;
            case "*": result = String.format("Result of multiplication is %d", a * b); break;
            case "/": result = b != 0 ? String.format("Result of division is %.2f", a*1.0 / b) : String.format("Error: division by zero"); break;
            default: result = String.format("Unsupported operation: %s", action);
        }
        System.out.println(result);
    }

    // Найти минимум и максимум в одномерном массиве. Найти минимум и максимум в двумерном массиве.
    public static void printArrayMinAndMax() {
        //Одномерный массив
        int[] array1dim = {0, 1, 2, 3, 4, 5, -1, -2, -3, 3};
        int min = array1dim[0];
        int max = array1dim[0];
        for (int i = 0; i < array1dim.length; i++) {
            if (array1dim[i] < min) min = array1dim[i];
            if (array1dim[i] > max) max = array1dim[i];
        }
        System.out.printf("One-dimension array min element: %d, max element: %d%n", min, max);
        //Двумерный массив
        int[][] array2dim = {   {0, 1, 2, 3, 4, 5, -1, -2, -3, 3},
                {12, 5, 1, 45, 11, -5},
                {11, 4, 22}                         };
        min = array2dim[0][0];
        max = array2dim[0][0];
        for (int i = 0; i < array2dim.length; i++) {
            for (int j = 0; j < array2dim[i].length; j++) {
                if (array2dim[i][j] < min) min = array2dim[i][j];
                if (array2dim[i][j] > max) max = array2dim[i][j];
            }
        }
        System.out.printf("Two-dimension array min element: %d, max element: %d%n", min, max);
    }

    // В двумерном массиве найти сумму всех элементов, ниже главной диагонали.
    public static void printArraySumUnderMainDiagonal(int size) {
        // Создаём двумерный массив размера size x size и выводим элементы на консоль по мере создания
        int[][] array = new int[size][size];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = (int)(Math.random()*25 - Math.random()*25);
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
        // Считаем сумму
        int sum = 0;
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < i; j++) {
                sum += array[i][j];
            }
        }
        System.out.printf("Sum of array's elements under the main diagonal is: %d%n", sum);
    }

    // Есть одномерный массив 1...31 (календарный месяц).
    // Число 1 соответствует понедельнику, число 2 - вторнику, число 8 - снова понедельник.
    // Найти числа всех пятниц в этом месяце. Определить день недели 31-го числа.
    public static void findAllFridaysAndDayOfWeek() {
        int[] calendar = new int[31];
        // День недели будем высчитывать по остатку от деления на 7, потому начинаем с нулевого элемента - воскресенья
        String[] daysOfWeek = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

        for (int i = 0; i < calendar.length; i++) {
            calendar[i] = i + 1;
        }

        for (int i = 4; i < calendar.length; i += 7) {
            System.out.printf("%d is %s!%n", calendar[i], daysOfWeek[5]);
        }

        String dayOf31th = daysOfWeek[31 % 7];
        System.out.printf("31 is %s!%n", dayOf31th);
    }


    public static void main(String[] args) throws IOException {
        System.out.printf("Min of three numbers: %d%n", minOfThree(12, 22, 5));
        System.out.printf("Factorial of %d by \"while\" statement: %d%n", 42, factorialMethod1(42));
        System.out.printf("Factorial of %d by \"do-while\" statement: %d%n", 42, factorialMethod1(42));
        System.out.printf("Factorial of %d by \"for\" statement: %d%n", 42, factorialMethod1(42));
        System.out.printf("Printing array elements without \"5\" element: %s%n", getArrayElementsExceptFive());
        System.out.println("Dummy calculator example. Enter an action from list [+ - * /]");
        dummyCalculator();
        System.out.println("Looking for Min and Max elements in one- and two-dimension array:");
        printArrayMinAndMax();
        System.out.println("Calculating sum of array's elements under the main diagonal:");
        printArraySumUnderMainDiagonal(5);
        System.out.println("Making fun with calendar:");
        findAllFridaysAndDayOfWeek();
    }

}
