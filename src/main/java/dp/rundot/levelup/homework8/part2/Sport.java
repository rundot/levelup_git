package dp.rundot.levelup.homework8.part2;

import java.util.List;

/**
 * Created by rundot on 20.10.2016.
 */
public class Sport<K extends Discipline, V extends Constituent> {

    private K discipline;
    private List<V> constituents;

    public Sport(K discipline, List<V> constituents) {
        this.discipline = discipline;
        this.constituents = constituents;
    }

    public void cantLiveWithout() {

        StringBuilder sb = new StringBuilder();

        if (constituents != null)
            for (Constituent constituent : constituents)
                if (constituent != null)
                    sb.append(constituent.about()).append(", ");

        if (sb.length() > 0)
            sb.delete(sb.length() - 2, sb.length());

        System.out.printf("%s It can't live without %s%n", discipline.description(), sb.toString());
    }

}
