package dp.rundot.levelup.homework8.part2;

/**
 * Created by rundot on 20.10.2016.
 */
public class Discipline {

    protected String name;
    protected String description;

    public String description() {
        return String.format("It's a %s. %s.", name, description);
    }

}
