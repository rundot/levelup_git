package dp.rundot.levelup.homework8.part2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by rundot on 20.10.2016.
 */
public class Run {

    public static void main(String[] args) {

        Football football = new Football("Football", "Game description");
        Hockey hockey = new Hockey("Hockey", "Game description");
        Formula formula = new Formula("Formula One", "Game description");

        Ball ball = new Ball("Round ball");
        Car car = new Car("Fast car");
        Ice ice = new Ice("Solid Ice");
        Fans fans = new Fans("Drunk fans");


        List<Constituent> footballConstituents = new ArrayList<>();
        Collections.addAll(footballConstituents, ball, fans);

//        Sport<Football, Ball> footballFirstPassion = new Sport<>(football, ball);
//        Sport<Hockey, Ice> hockeyIceSport = new Sport<>(hockey, ice);
//        footballFirstPassion.cantLiveWithout();
//        hockeyIceSport.cantLiveWithout();

        Character.toUpperCase('a');

        Sport<Football, Constituent> footballPassion = new Sport<>(football, footballConstituents);
        footballPassion.cantLiveWithout();

    }

}
