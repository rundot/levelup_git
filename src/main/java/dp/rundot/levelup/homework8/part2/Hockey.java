package dp.rundot.levelup.homework8.part2;

/**
 * Created by rundot on 20.10.2016.
 */
public class Hockey extends Discipline {

    public Hockey(String name, String description) {
        this.name = name;
        this.description = description;
    }

}
