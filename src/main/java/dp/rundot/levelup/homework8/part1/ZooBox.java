package dp.rundot.levelup.homework8.part1;

/**
 * Created by rundot on 20.10.2016.
 */
public class ZooBox<T extends Animal> {

    T animal;
    public void lockAnimal(T animal) {
        this.animal = animal;
    }

    public T getAnimal() {
        return this.animal;
    }

}
