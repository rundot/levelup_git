package dp.rundot.levelup.homework8.part1;

/**
 * Created by rundot on 20.10.2016.
 */
public class Shark extends Animal {

    public Shark(String name) {
        this.name = name;
        this.animalType = AnimalType.FISH;
    }

    @Override
    public void sayName() {
        System.out.printf("I'm a %s. My name is %s.%n", animalType, name);
    }

}
