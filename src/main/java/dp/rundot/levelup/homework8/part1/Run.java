package dp.rundot.levelup.homework8.part1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rundot on 20.10.2016.
 */
public class Run {

    public static void main(String[] args) {

        //Creating animals
        Bear bear = new Bear("Vadim");
        Monkey monkey = new Monkey("Julia");
        Shark shark = new Shark("Anatoly");
        Zebra zebra = new Zebra("Fedor");

        //Making special boxes
        ZooBox<Bear> bearZooBox = new ZooBox<>();
        ZooBox<Monkey> monkeyZooBox = new ZooBox<>();
        ZooBox<Shark> sharkZooBox = new ZooBox<>();
        ZooBox<Zebra> zebraZooBox = new ZooBox<>();

        //Locking them in
        bearZooBox.lockAnimal(bear);
        monkeyZooBox.lockAnimal(monkey);
        sharkZooBox.lockAnimal(shark);
        zebraZooBox.lockAnimal(zebra);

        //Make them crying out from the boxes
        bearZooBox.getAnimal().sayName();
        monkeyZooBox.getAnimal().sayName();
        sharkZooBox.getAnimal().sayName();
        zebraZooBox.getAnimal().sayName();

        //Making universal box for any animal
        ZooBox<Animal> animalZooBox = new ZooBox<>();

        //Making list of boxes
        List<ZooBox<? extends Animal>> list = new ArrayList<>();

        //Adding boxes to the list
        list.add(bearZooBox);
        list.add(monkeyZooBox);
        list.add(sharkZooBox);
        list.add(zebraZooBox);
        list.add(animalZooBox);

        //Let them cry out from the boxes inside the list
        list.forEach((ZooBox zooBox) -> {zooBox.getAnimal().sayName();});

    }

}
