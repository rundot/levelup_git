package dp.rundot.levelup.homework8.part1;

/**
 * Created by rundot on 20.10.2016.
 */
public enum AnimalType {

    MAMMAL,
    AMPHIBIAN,
    BIRD,
    FISH;
}
