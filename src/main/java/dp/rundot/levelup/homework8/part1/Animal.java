package dp.rundot.levelup.homework8.part1;

/**
 * Created by rundot on 20.10.2016.
 */
public abstract class Animal {

    protected AnimalType animalType;
    protected String name;

    public abstract void sayName();


}
