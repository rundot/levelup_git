package dp.rundot.levelup.homework8.part1;

/**
 * Created by rundot on 20.10.2016.
 */
public class Zebra extends Animal {

    public Zebra(String name) {
        this.name = name;
        this.animalType = AnimalType.MAMMAL;
    }

    @Override
    public void sayName() {
        System.out.printf("I'm a %s. My name is %s.%n", animalType, name);
    }

}
