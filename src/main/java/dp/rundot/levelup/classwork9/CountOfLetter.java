package dp.rundot.levelup.classwork9;

import java.net.Inet4Address;
import java.util.*;

/**
 * Created by emaksimovich on 21.10.16.
 */
public class CountOfLetter {

    private static class ValueComparator implements Comparator {

        private Map<Character, Integer> map;

        public ValueComparator (Map<Character, Integer> map) {
            this.map = map;
        }

        @Override
        public int compare(Object o1, Object o2) {
            int result = map.get(o2).compareTo(map.get(o1));
            if (result == 0)
                return ((Character)o2).compareTo((Character)o1);
            else return result;
        }
    }

    public static void countOfLetter(String string) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < string.length(); i++) {
            map.put(string.charAt(i), map.get(string.charAt(i))==null? 1 : map.get(string.charAt(i)) + 1);
        }

        Map<Character, Integer> sortedMap = new TreeMap<>(new ValueComparator(map));
        sortedMap.putAll(map);
        sortedMap.forEach((ch, num) -> {
            System.out.printf("%s: %d entries%n", ch, num);
        });
    }

    public static void main(String[] args) {
        String phrase = "midway upon the journey of our life i found myself within a forest dark";
        countOfLetter(phrase);
    }

}
