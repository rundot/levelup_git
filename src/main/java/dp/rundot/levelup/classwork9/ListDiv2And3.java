package dp.rundot.levelup.classwork9;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 21.10.16.
 */
public class ListDiv2And3 {

    public static List<Integer> change(List<Integer> list) {
        (new ArrayList<Integer>(list)).forEach(num -> {
            if (num % 2 == 0 && num % 3 != 0) list.remove(num);
            if (num % 3 == 0 && num %2 != 0) list.set(list.indexOf(num), num * 2);
        });
        return list;
    }

}
