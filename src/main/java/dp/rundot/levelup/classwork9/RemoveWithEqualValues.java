package dp.rundot.levelup.classwork9;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by emaksimovich on 23.10.16.
 */
public class RemoveWithEqualValues {

    public static <K, V> void removeItemFromMapByValue(Map<K, V> map, V value) {
        (new TreeMap<K, V>(map)).forEach(((k, v) -> {if (v.equals(value)) map.remove(k);}));
    }

    public static <K, V> void removeEqualValues(Map<K, V> map) {
        Map<V, K> uniqueValues = new TreeMap<>();
        (new TreeMap<>(map)).forEach((k, v) -> {
            if (uniqueValues.get(v) != null) {
                map.remove(k);
                map.remove(uniqueValues.get(v));
                uniqueValues.remove(v);
            }
            else uniqueValues.put(v, k);
        });
    }

    public static <K, V> void removeNotFirstEqualValues(Map<K, V> map) {
        List<V> uniqueValues = new ArrayList<>();
        (new TreeMap<>(map)).forEach((k, v) -> {
            if (uniqueValues.contains(v))
                map.remove(k);
            else uniqueValues.add(v);
        });
    }

}
