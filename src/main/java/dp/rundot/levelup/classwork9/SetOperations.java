package dp.rundot.levelup.classwork9;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by emaksimovich on 21.10.16.
 */
public class SetOperations {

    public static <T> boolean isSubset(Set<T> set1, Set<T> set2) {
        return set2.containsAll(set1);
    }

    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set set = new HashSet<>();
        Collections.addAll(set, set1, set1);
        return set;
    }

    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>();
        set1.forEach((element) -> {if (set2.contains(element)) set.add(element);});
        return set;
    }

    public static <T> Set<T> SymDifference(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>();
        set1.forEach((element) -> {if (!set2.contains(element)) set.add(element);});
        set2.forEach((element) -> {if (!set1.contains(element)) set.add(element);});
        return set;
    }

    public static <T> Set<T> difference(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<T>();
        set1.forEach((element) -> {if (!set2.contains(element)) set.add(element);});
        return set;
    }

}
