package dp.rundot.levelup.classwork9;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by emaksimovich on 21.10.16.
 */
public class Countdown {

    public static void main(String[] args) {
        List<Integer> queue = new LinkedList<>();
        for (int i = 10; i >= 0; i--) {
            queue.add(i);
        }
        while (!queue.isEmpty()) {
            System.out.printf("removing %d%n", queue.remove(0));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
