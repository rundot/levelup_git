package dp.rundot.levelup.homework12.part3;

/**
 * Created by emaksimovich on 31.10.16.
 */
public class BigMacMenu {

    private boolean isHamburgerReady;
    private boolean isFriesReady;
    private boolean isColaReady;
    private boolean isBigMacMenuReady;

    public boolean isHamburgerReady() {
        return isHamburgerReady;
    }

    public void setHamburgerReady(boolean hamburgerReady) {
        isHamburgerReady = hamburgerReady;
    }

    public boolean isFriesReady() {
        return isFriesReady;
    }

    public void setFriesReady(boolean friesReady) {
        isFriesReady = friesReady;
    }

    public boolean isColaReady() {
        return isColaReady;
    }

    public void setColaReady(boolean colaReady) {
        isColaReady = colaReady;
    }

    public boolean isBigMacMenuReady() {
        return isBigMacMenuReady;
    }

    public void setBigMacMenuReady(boolean bigMacMenuReady) {
        isBigMacMenuReady = bigMacMenuReady;
    }
}
