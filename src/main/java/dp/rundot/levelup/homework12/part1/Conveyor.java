package dp.rundot.levelup.homework12.part1;

/**
 * Created by emaksimovich on 31.10.16.
 */
public class Conveyor {

    //Create two workers; Make second worker wait until first worker finished his work;

    public static void main(String[] args) {
        Worker workerVasya = new Worker("Vasya", "Thread2");
        Worker workerPetya = new Worker("Petya", "Thread1");

        workerVasya.start();
        try {
            workerVasya.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        workerPetya.start();
    }

}
