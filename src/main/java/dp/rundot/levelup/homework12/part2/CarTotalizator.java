package dp.rundot.levelup.homework12.part2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 31.10.16.
 */
public class CarTotalizator {

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int luckyNumber = 0;

        while (true) {
            System.out.print("Enter lucky number (1 to 10):\t");
            try {
                luckyNumber = Integer.parseInt(reader.readLine());
            } catch (Exception e) {
            }
            if (luckyNumber >= 1 && luckyNumber <= 10) break;
            System.out.println("Incorrect number");
        }

        List<Car> carList = new ArrayList<>();

        for (int i = 1; i <= 10; i++) {
            Car car = new Car(i);
            if (i == luckyNumber)
                car.setPriority(Thread.MAX_PRIORITY);
                else car.setPriority(Thread.MIN_PRIORITY);
            carList.add(car);
        }

        carList.stream().forEach(Car::start);

    }


}
