package dp.rundot.levelup.homework12.part4;

/**
 * Created by rundot on 01.11.2016.
 */
public class Account {

    private Long balance = 0l;

    public Account(Long balance) {
        this.balance = balance;
    }

    public synchronized void add(Long money) {
        System.out.printf("Adding %d by %s...%n", money, Thread.currentThread().getName());
        balance += money;
        System.out.printf("Succeed. Total amount: %d%n", balance);
    }

    public synchronized void withdraw(Long money) {
        System.out.printf("Withdrawing %d by %s...%n", money, Thread.currentThread().getName());
        if (money <= balance) {
            balance -= money;
            System.out.printf("Succeed. Total amount: %d%n", balance);
        } else {
            System.out.println("Insufficient money");
        }
    }

}
