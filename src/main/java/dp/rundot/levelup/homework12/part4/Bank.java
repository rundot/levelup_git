package dp.rundot.levelup.homework12.part4;

/**
 * Created by rundot on 01.11.2016.
 */
public class Bank {

    public static Account account = new Account(1000l);
    public static volatile boolean isBankomatWorking = false;

    public static void main(String[] args) {

        isBankomatWorking = true;

        AddMoney adder = new AddMoney();
        WithdrawMoney withdrawer1 = new WithdrawMoney();
        WithdrawMoney withdrawer2 = new WithdrawMoney();
        WithdrawMoney withdrawer3 = new WithdrawMoney();
        WithdrawMoney withdrawer4 = new WithdrawMoney();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        isBankomatWorking = false;
    }

}
