package dp.rundot.levelup.homework12.part4;

/**
 * Created by rundot on 01.11.2016.
 */
public class AddMoney extends Thread {

    public AddMoney() {
        start();
    }

    @Override
    public void run() {
        while (Bank.isBankomatWorking) {
            Bank.account.add(1000l);
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
