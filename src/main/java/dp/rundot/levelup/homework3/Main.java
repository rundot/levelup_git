package dp.rundot.levelup.homework3;

/**
 * Created by rundot on 08.11.2016.
 */
public class Main {


    /*Находит минимальное из трех чмсел*/
    public static int minOfThree(int a, int b, int c) {
        //Неправильная проверка в первом if, должно быть (a < b)
        if (a > b)
            if (a < c) return a;
            else return c;
        else if (b < c) return b;
        else return c;
    }

    /*вычисляет факториал заданного числа*/
    public static int factorial(int n) {
        //Нет проверки на отрицательный аргумент
        int fact = 1;
        while (n > 0) {
            fact = fact * n;
            n++;
        }
        return fact;
    }

    /*Сравнивает две строки*/
    public static boolean compareString(String firstString, String secondString) {
        //Строки сравниваются по ссылке, а не по содержанию
        return firstString == secondString;
    }

    /*Склеивает переданные строки в одну*/
    public static String concatenateString(String... strings) {
        //Строки не соединяются
        //Нет проверки на null в качестве аргумента
        String result = "";
        for (String s : strings)
            result = s;
        return result;
    }

    /*Находит минимум в одномерном массиве*/
    public static int findMinInArray(int[] array) {
        //Выходим за границы массива при i = array.length
        //Нет проверки аргумента на null
        int min = array[0];
        for (int i = 1; i <= array.length; i++) if (array[i] < min) min = array[i];
        return min;
    }

    /*сортирует массив по возрастанию*/
    public static int[] sortArray(int[] array) {
        //Выход за границы массива в [j + 1] при j = array.length - 1
        //Сортировка по убыванию вместо сортировки по возрастанию
        //Нет проверки аргумента на null
        for (int i = array.length; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] < array[j + 1]) {
                    int buffer = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = buffer;
                }
            }
        }
        return array;
    }

    /*Находит сумму элементов, находящихся ниже главной диагонали*/
    public static int sumOfElementsUnderMainDiagonal(int[][] matrix) {
        //Нет проверки на корректность матрицы
        //Нет проверки на равенство аргумента null
        //Неправильное условие выхода из цикла, учитываются элементы, входящие в диагональ. Должно быть (j >= i)
        int sum = 0;
        for (int i = 1; i < matrix.length; i++)
            for (int j = 0; j < matrix.length; j++) {
                if (j > i) break;
                sum += matrix[i][j];
            }
        return sum;
    }

    /*Находит количество чисел, являющихся степенями двойки в матрице*/
    public static int countPowerOfTwo(int[][] matrix) {
        //Нет проверки на корректность матрицы
        //Нет проверки на равенство аргумента null
        //Неправильное условие проверки. Должно быть "&" вместо "|"
        //Кроме того, нет проверки элемента на 0. Так как, технически, для 0 и -1 выполнится равенство 0 & -1 == 0, хотя 0 не является степенью двойки
        int count = 0;
        for (int i = 0; i < matrix.length; i++)
            for (int j = 0; j < matrix[0].length; j++)
                if ((matrix[i][j] | (matrix[i][j] - 1)) == 0) count++;

        return count;
    }

    /*Метод длится 1000 миллисекунд*/
    public static void sleepFor1000() throws InterruptedException {
        //Здесь всё в порядке, просто засыпаем текущий тред на 1 сек
        Thread.sleep(1000);
    }

    /*Метод бросает исключение*/
    public static void throwException() throws Exception {
        //Здесь тоже всё ок. Бросаем Эксепшн. Что ещё может пойти не так?
        throw new Exception();
    }

}
