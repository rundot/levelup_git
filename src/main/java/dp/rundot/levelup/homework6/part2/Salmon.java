package dp.rundot.levelup.homework6.part2;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public class Salmon extends Animal implements Swimable{

    public Salmon(String name, String color) {
        this.name = name;
        this.color = color;
        this.countOfWings = 0;
        this.countOfWings = 0;
        this.type = AnimalType.FISH;
    }

    @Override
    public String swim() {
        return "I can swim! ";
    }

    @Override
    void voice() {
        String action = swim();
        System.out.printf("My name is %s. My color is %s. I have %d legs. I have %d wings. %sWho am I?%n", name, color, countOfLegs, countOfWings, action);
    }
}
