package dp.rundot.levelup.homework6.part2;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public class Run {

    public static void main(String[] args) {
        Cat cat = new Cat("Captain Furry Paws", "Black");
        Lizard lizard = new Lizard("Godzilla", "Toxic");
        Salmon salmon = new Salmon("Arcadiy", "Silver");
        Sparrow sparrow = new Sparrow("Harpy", "Brownish");
        Animal[] animals = {cat, lizard, salmon, sparrow};
        for (Animal animal : animals) {
            animal.voice();
        }
    }

}
