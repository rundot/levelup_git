package dp.rundot.levelup.homework6.part2;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public abstract class Animal {

    protected String name;
    protected int countOfLegs;
    protected int countOfWings;
    protected String color;
    protected AnimalType type;

    abstract void voice();

}
