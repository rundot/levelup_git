package dp.rundot.levelup.homework6.part2;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public class Lizard extends Animal implements Runnable, Swimable {

    public Lizard(String name, String color) {
        this.name = name;
        this.color = color;
        this.countOfWings = 0;
        this.countOfLegs = 4;
        this.type = AnimalType.AMPHIBIAN;
    }

    @Override
    public String run() {
        return "I can run! ";
    }

    @Override
    public String swim() {
        return "I can swim! ";
    }

    @Override
    void voice() {
        String action = run() + swim();
        System.out.printf("My name is %s. My color is %s. I have %d legs. I have %d wings. %sWho am I?%n", name, color, countOfLegs, countOfWings, action);
    }
}
