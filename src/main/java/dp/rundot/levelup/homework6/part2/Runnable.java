package dp.rundot.levelup.homework6.part2;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public interface Runnable {

    String run();

}
