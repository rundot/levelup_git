package dp.rundot.levelup.homework6.part2;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public class Sparrow extends Animal implements Flyable {

    public Sparrow(String name, String color) {
        this.name = name;
        this.color = color;
        this.countOfLegs = 2;
        this.countOfWings = 2;
        this.type = AnimalType.BIRD;
    }

    @Override
    public String fly() {
        return "I can fly! ";
    }

    @Override
    void voice() {
        String action = fly();
        System.out.printf("My name is %s. My color is %s. I have %d legs. I have %d wings. %sWho am I?%n", name, color, countOfLegs, countOfWings, action);
    }
}
