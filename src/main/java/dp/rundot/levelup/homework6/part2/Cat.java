package dp.rundot.levelup.homework6.part2;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public class Cat extends Animal implements Runnable {

    public Cat(String name, String color) {
        this.name = name;
        this.color = color;
        this.countOfLegs = 4;
        this.countOfWings = 0;
        this.type = AnimalType.MAMMAL;
    }

    @Override
    void voice() {
        String action = run();
        System.out.printf("My name is %s. My color is %s. I have %d legs. I have %d wings. %sWho am I?%n", name, color, countOfLegs, countOfWings, action);
    }

    @Override
    public String run() {
        return "I can run! ";
    }
}
