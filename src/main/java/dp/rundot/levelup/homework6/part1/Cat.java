package dp.rundot.levelup.homework6.part1;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public class Cat extends Pet {

    @Override
    public void voice() {
        System.out.printf("I am %s. My name is %s. My voice is meow!%n", type, name);
    }

    public Cat(String name) {
        this.type = "cat";
        this.name = name;
    }


}
