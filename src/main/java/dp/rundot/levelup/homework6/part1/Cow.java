package dp.rundot.levelup.homework6.part1;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public class Cow extends Pet {

    @Override
    public void voice() {
        System.out.printf("I am %s. My name is %s. My voice is moo!%n", type, name);
    }

    public Cow(String name) {
        this.type = "cow";
        this.name = name;
    }


}
