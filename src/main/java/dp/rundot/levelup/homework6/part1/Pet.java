package dp.rundot.levelup.homework6.part1;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public abstract class Pet {

    public String name;
    public String type;

    public abstract void voice();

}
