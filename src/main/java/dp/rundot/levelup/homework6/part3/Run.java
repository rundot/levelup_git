package dp.rundot.levelup.homework6.part3;

/**
 * Created by Maksimovich Evgeniy on 09.10.2016.
 */
public class Run {

    public static void main(String[] args) {
        Phone phone1 = new Phone("Apple", "6S", 1, true, "Deep Blue", PhoneType.SENSOR);
        Phone phone2 = new Phone("Apple", "6S", 1, true, "Gold", PhoneType.SENSOR);
        Phone phone3 = new Phone("Siemens", "C35", 1, false, "Black", PhoneType.BLOCK);
        Shop shop = new Shop();
        shop.newDelivery(new Phone[] {phone1, phone2, phone3});
        shop.printPhonesInStock();
        Phone phone4 = new Phone("Apple", "6S", 1, true, "White", PhoneType.SENSOR);
        System.out.printf("Looking for: %s%n", phone4);
        System.out.printf("%d found.%n", shop.countOfSearchPhone(phone4));
    }

}
