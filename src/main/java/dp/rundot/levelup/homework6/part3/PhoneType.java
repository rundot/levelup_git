package dp.rundot.levelup.homework6.part3;

/**
 * Created by Maksimovich Evgeniy on 09.10.2016.
 */
public enum PhoneType {

    SLIDER,
    BLOCK,
    SENSOR,
    FLIP;

}
