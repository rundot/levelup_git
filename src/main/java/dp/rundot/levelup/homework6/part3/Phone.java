package dp.rundot.levelup.homework6.part3;

/**
 * Created by Maksimovich Evgeniy on 08.10.2016.
 */
public class Phone {

    private String producer;
    private String model;
    private int numberOfSim = 1;
    private boolean hasCamera;
    private String color;
    private PhoneType phoneType;

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setNumberOfSim(int numberOfSim) {
        this.numberOfSim = numberOfSim;
    }

    public void setCamera(boolean hasCamera) {
        this.hasCamera = hasCamera;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
    }

    public Phone(String producer, String model, int numberOfSim, boolean hasCamera, String color, PhoneType phoneType) {
        this.producer = producer;
        this.model = model;
        this.numberOfSim = numberOfSim;
        this.hasCamera = hasCamera;
        this.phoneType = phoneType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone = (Phone) o;

        if (numberOfSim != phone.numberOfSim) return false;
        if (hasCamera != phone.hasCamera) return false;
        if (producer != null ? !producer.equals(phone.producer) : phone.producer != null) return false;
        if (model != null ? !model.equals(phone.model) : phone.model != null) return false;
        return phoneType == phone.phoneType;

    }

    @Override
    public String toString() {
        return "Phone{" +
                "producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", numberOfSim=" + numberOfSim +
                ", hasCamera=" + hasCamera +
                ", color='" + color + '\'' +
                ", phoneType=" + phoneType +
                '}';
    }

    @Override
    public int hashCode() {
        int result = producer != null ? producer.hashCode() : 0;
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + numberOfSim;
        result = 31 * result + (hasCamera ? 1 : 0);
        result = 31 * result + (phoneType != null ? phoneType.hashCode() : 0);
        return result;
    }
}
