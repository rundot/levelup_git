package dp.rundot.levelup.homework6.part3;

import java.util.*;

/**
 * Created by Maksimovich Evgeniy on 09.10.2016.
 */
public class Shop {

    private Phone[] phonesInStock = new Phone[0];

    public Phone[] getPhonesInStock() {
        return phonesInStock;
    }

    public void newDelivery(Phone[] phonesDelivery) {
        if (phonesDelivery == null) return;
        Phone[] temp = new Phone[phonesInStock.length + phonesDelivery.length];
        for (int i = 0; i < phonesInStock.length; i++) {
            temp[i] = phonesInStock[i];
        }
        for (int i = 0; i < phonesDelivery.length; i++) {
            temp[phonesInStock.length + i] = phonesDelivery[i];
        }
        phonesInStock = temp;
    }

    public int countOfSearchPhone(Phone searchPhone) {
        if (searchPhone == null) return 0;
        int found = 0;
        for (int i = 0; i < phonesInStock.length; i++) {
            if (phonesInStock[i].equals(searchPhone)) found++;
        }
        return found;
    }

    public void printPhonesInStock() {
        Map<Phone, Integer> phones = new HashMap<>();
        for (Phone phone : phonesInStock) {
            int qty = phones.get(phone) == null ? 0 : phones.get(phone);
            phones.put(phone, ++qty);
        }
        System.out.println("Stock contains");
        for (Map.Entry<Phone, Integer> entry : phones.entrySet()) {
            System.out.printf("%s : %s pcs.%n", entry.getKey(), entry.getValue());
        }
    }
}
