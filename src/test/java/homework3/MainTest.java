package homework3;

import dp.rundot.levelup.homework3.Main;
import org.junit.Test;
import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void testMinOfThreeNormalFlow() {
        assertEquals(0, Main.minOfThree(2, 0, 1));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testFactorialExceptionThrows() throws IllegalArgumentException {
        Main.factorial(-1);
    }

    @Test
    public void testCompareStringNormalFlow() {
        assertTrue(Main.compareString(new String("abc"), new String("abc")));
    }

    @Test
    public void testConcatenateStringNormalFlow() {
        assertEquals("abc", Main.concatenateString("a", "b", "c"));
    }

    @Test (expected =  IllegalArgumentException.class)
    public void testConcatenateStringExceptionThrows() throws IllegalArgumentException {
        System.out.println(Main.concatenateString(null));
    }

    @Test
    public void testFindMinInArrayNormalFlow() {
        int[] array = {1, 2, 3, 4, 5};
        assertEquals(1, Main.findMinInArray(array));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testFindMinInArrayExceptionThrows() throws IllegalArgumentException {
        Main.findMinInArray(null);
    }

    @Test
    public void testSortArrayNormalFlow() {
        int[] unsortedArray = {5, 2, 1, 3, 4};
        int[] sortedArray = {1, 2, 3, 4, 5};
        assertArrayEquals(sortedArray, Main.sortArray(unsortedArray));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSortArrayExceptionThrows() throws IllegalArgumentException {
        Main.sortArray(null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSumOfElementsUnderMainDiagonalNullArgumentExceptionThrows() throws IllegalArgumentException {
        Main.sumOfElementsUnderMainDiagonal(null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSumOfElementsUnderMainDiagonalIncorrectMatrixExceptionThrows() throws IllegalArgumentException {
        int[][] array = {   {},
                null,
                {1, 2}   };
        Main.sumOfElementsUnderMainDiagonal(array);
    }

    @Test
    public void testSumOfElementsUnderMainDiagonalNormalFlow() {
        int[][] array = {   {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}  };
        assertEquals(19, Main.sumOfElementsUnderMainDiagonal(array));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testCountPowerOfTwoNullArgumentExceptionThrows() throws IllegalArgumentException {
        Main.countPowerOfTwo(null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testCountPowerOfTwoIncorrectMatrixExceptionThrows() throws IllegalArgumentException {
        int[][] array = {   {},
                null,
                {1, 2}   };
        Main.countPowerOfTwo(array);
    }

    @Test
    public void testCountPowerOfTwoNormalFlow() {
        int[][] array = {   {0, 2, 4},
                {8, 15, 16},
                {32, 64, 127}   };
        assertEquals(6, Main.countPowerOfTwo(array));
    }

    @Test (timeout = 1100l)
    public void testSleepFor1000NormalFlow() throws InterruptedException {
        Main.sleepFor1000();
    }

    @Test (expected = Exception.class)
    public void testThrowExceptionNormalFlow() throws Exception {
        Main.throwException();
    }

}