package classwork4;

import dp.rundot.levelup.classwork4.QuickSort;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class TestQuickSort {

    int[] array = new int[20];

    @Before
    public void createArray() {
        array = new int[20];
        System.out.print("Unsorted array: ");
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            array[i] = random.nextInt(100);
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    @After
    public void printArray() {
        System.out.print("Sorted array: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    @Test
    public void testPartitionNormalFlow() {
        int midIndex = QuickSort.partition(array, 0, array.length - 1);
        boolean result = true;
        for (int i = 0; i < midIndex; i++) {
            if (array[i] > array[midIndex]) result = false;
        }
        for (int i = midIndex + 1; i < array.length; i++) {
            if (array[i] < array[midIndex]) result = false;
        }
        assertTrue(result);
    }

    @Test
    public void testSortFullArrayNormalFlow() {
        QuickSort.sort(array);
        boolean sorted = true;
        for (int i = 1; i < array.length; i++) {
            if (array[i] < array[i - 1]) {
                sorted = false;
            }
        }
        assertTrue(sorted);
    }

    @Test
    public void testSortPartialArrayNormalFlow() {
        QuickSort.sort(array, 5, 15);
        boolean sorted = true;
        for (int i = 6; i <= 15; i++) {
            if (array[i] < array[i - 1]) {
                sorted = false;
            }
        }
        assertTrue(sorted);
    }

}