package dp.rundot.levelup.homework5.part1;

import org.junit.Test;

/**
 * Created by Maksimovich Evgeniy on 03.10.2016.
 */
public class StudentTest {

    @Test
    public void testStudentNormalFlow() {
        System.out.println("Entered a college");
        Student spiderMan = new Student("Peter Parker", 17, true, 58, 0);
        System.out.println(spiderMan);
        System.out.println("Finished first year of the college");
        spiderMan.setYearOfStudy(1);
        System.out.println(spiderMan);
        spiderMan.incYearOfStudy(4);
        System.out.println("Finished the college");
        System.out.println(spiderMan);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testStudentExceptionFlow() {
        Student robin = new Student("Dick Grason", 19, true, 72, 4);
        robin.incYearOfStudy(-2);
    }

}
