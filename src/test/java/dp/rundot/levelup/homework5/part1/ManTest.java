package dp.rundot.levelup.homework5.part1;

/**
 * Created by Maksimovich Evgeniy on 03.10.2016.
 */

import org.junit.Test;

public class ManTest {

    @Test
    public void testManNormalFlow() {
        Man captainAmerica = new Man("Steve Rogers", 90, true, 90);
        Man blackWidow = new Man("Natasha Romanoff", 28, false, 52);
        Man bruceBanner = new Man();
        bruceBanner.setName("Bruce Banner");
        bruceBanner.setAge(35);
        bruceBanner.setWeight(84);
        System.out.println(captainAmerica);
        System.out.println(blackWidow);
        System.out.println(bruceBanner);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testManThrowsException() {
        Man impossibleMan = new Man();
        impossibleMan.setName(null);
        impossibleMan.setAge(-20);
        impossibleMan.setWeight(-300);
    }

}
