package dp.rundot.levelup.homework5.part2;

import org.junit.Test;

/**
 * Created by Maksimovich Evgeniy on 03.10.2016.
 */
public class MatrixTest {

    //Normal matrix creation - all tests should pass
    @Test
    public void testMatrixCreationNormalFlow() {
        int[][] matrixArray = {{10, 20, 50}, {30, 10, 0}, {12, 24, 36}};
        //Creating from array
        Matrix matrix1 = new Matrix(matrixArray);
        //Random generation with given width and height
        Matrix matrix2 = new Matrix(4, 6);
        System.out.println(matrix1);
        System.out.println(matrix2);
    }

    //Creating matrix from incorrect arguments. Test should throw an exception
    @Test (expected =  IllegalArgumentException.class)
    public void testMatrixCreationExceptionThrows() {
        Matrix matrix1 = new Matrix(null);
        Matrix matrix2 = new Matrix(-3, -4);
        Matrix matrix3 = new Matrix(new int[][] {{1, 2}, null, {0}});

    }

    //Testing normal flow of matrix addition
    @Test
    public void testMatrixAdditionNormalFlow() {
        int[][] matrix1Array = {{1, 2}, {3, 4}};
        int[][] matrix2Array = {{5, 6}, {7, 8}};
        Matrix matrix1 = new Matrix(matrix1Array);
        Matrix matrix2 = new Matrix(matrix2Array);
        matrix1.add(matrix2);
        System.out.println(matrix1);
    }

    //Testing addition of matrix with different dimensions
    @Test (expected = IllegalArgumentException.class)
    public void testMatrixAdditionExceptionThrows() {
        Matrix matrix1 = new Matrix(new int[][] {{1, 2}, {3, 4}});
        Matrix matrix2 = new Matrix(new int[][] {{1, 2}, {3, 4}, {5, 6}});
        matrix1.add(matrix2);
    }

    //Testing multiplication by a number. What could go wrong?
    @Test
    public void testMultiplicationByNumberNormalFlow() {
        Matrix matrix = new Matrix(new int[][] {{1, 2}, {3, 4}});
        matrix.multiply(12);
        System.out.println(matrix);
    }

    //Testing transposing. What could go wrong?
    @Test
    public void testTransposeNormalFlow() {
        Matrix matrix = new Matrix(5, 3);
        System.out.println("Original matrix\n" + matrix);
        matrix.transpose();
        System.out.println("Transponed matrix\n" + matrix);
    }

    //Testing normal flow of matrix multiplication
    @Test
    public void testMultiplicationNormalFlow() {
        Matrix matrix1 = new Matrix(new int[][] {{2, 1}, {-3, 0}, {4, -1}});
        Matrix matrix2 = new Matrix(new int[][] {{5, -1, 6}, {-3, 0, 7}});
        matrix1.multiply(matrix2);
        System.out.println(matrix1);
    }

    //Testing multiplication of wrong dimension matrix. Should throw exception.
    @Test (expected =  IllegalArgumentException.class)
    public void testMultiplicationExceptionThrows() {
        Matrix matrix1 = new Matrix(2, 2);
        Matrix matrix2 = new Matrix(4, 6);
        matrix1.multiply(matrix2);
    }

    //Whoooooaaahhhh! Let hardcore begins!
    @Test
    public void testHardcore() {
        Matrix matrix1 = new Matrix(3, 6);
        Matrix matrix2 = new Matrix(6, 3);
        Matrix matrix3 = new Matrix(2, 3);
        Matrix matrix4 = new Matrix(6, 2);
        Matrix matrix5 = matrix1.transpose().add(matrix2).transpose().multiply(matrix3).transpose().add(matrix4).transpose();
        System.out.println(matrix5);
    }

}
