package homework12;

/**
 * Created by Maksimovich Evgeniy on 01.11.2016.
 */

import dp.rundot.levelup.homework12.part5.MatrixHelper;
import dp.rundot.levelup.homework12.part5.ThreadMultiply;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestThreadMultiply {

    @Test
    public void testMatrixMultiplication() {
        long[][] a = MatrixHelper.fillMatrix(new long[1000][1000]);
        long[][] b = MatrixHelper.fillMatrix(new long[1000][1000]);
        long[][] result1Thread = ThreadMultiply.multiplyParallel(a, b, 1);
        long[][] result16Threads = ThreadMultiply.multiplyParallel(a, b, 16);

        assertArrayEquals(result1Thread, result16Threads);
    }

}
