package homework11;

import dp.rundot.levelup.homework11.PhoneBook;
import dp.rundot.levelup.homework11.Record;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

/**
 * Created by Maksimovich Evgeniy on 31.10.2016.
 */
public class TestPhoneBook {

    @Test
    public void testPhoneBookSerialization() throws IOException, ClassNotFoundException {
        PhoneBook phoneBook = new PhoneBook();
        Record record = new Record();
        record.setFirstName("John");
        record.setSecondName("Abraham");
        record.setSurname("Snow");
        record.setAddress("Westeros, Wall Street, 7");
        record.setPhoneNumber("+111123456789");
        record.createFullName();

        phoneBook.addRecord(record);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(buffer);
        out.writeObject(phoneBook);

        ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buffer.toByteArray()));

        PhoneBook restoredPhoneBook = (PhoneBook)in.readObject();

        assertEquals(phoneBook.toString(), restoredPhoneBook.toString());


    }

}
