package homework11;


import dp.rundot.levelup.homework11.Record;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

/**
 * Created by Maksimovich Evgeniy on 30.10.2016.
 */
public class TestRecord {

    @Test
    public void testRecordSerialization() throws IOException, ClassNotFoundException {
        Record record = new Record();
        record.setFirstName("John");
        record.setSecondName("Abraham");
        record.setSurname("Snow");
        record.setAddress("Westeros, Wall Street, 7");
        record.setPhoneNumber("+111123456789");
        record.createFullName();

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(buffer);
        out.writeObject(record);

        ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buffer.toByteArray()));
        Record restoredRecord = (Record)in.readObject();

        assertEquals(record, restoredRecord);
    }

}
